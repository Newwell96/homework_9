var express = require('express');
var router = express.Router();

const authMiddleware = require("../middleware/auth")

router.use(authMiddleware)

router.get('/me', function (req, res, next) {
  res.send({login: "test"})
});

module.exports = router;
