import React from 'react';
import '../../../index.css';
import scoresData from "./data/scoresData.json"
import ScoresRow from "./components/scoresRow";

const scoreList = scoresData["scoresList"]

function Scores () {

    function renderRows (data) {
        return data.map(
            item => (
                <ScoresRow
                    name = {item["name"]}
                    total = {item["totalGames"]}
                    wins= {item["wins"]}
                    loses = {item["loses"]}
                    percent = {item["percentWins"]}
                />
            )
        )
    }

    return (
        <div className="tables_body">
            <div id="tables_container">
                <h1 className="scores_title">Рейтинг игроков</h1>
                <table className="table">
                    <thead>
                    <tr className="score_row">
                        <th className="first_colomn">
                            ФИО
                        </th>
                        <th className="tipic_colomn">
                            Всего игр
                        </th>
                        <th className="tipic_colomn">
                            Победы
                        </th>
                        <th className="tipic_colomn">
                            Проигрыши
                        </th>
                        <th className="last_colomn">
                            Процент побед
                        </th>
                    </tr>

                    </thead>
                    <tbody>

                    {renderRows(scoreList)}

                    </tbody>
                </table>
            </div>
        </div>
)
}

export default Scores;