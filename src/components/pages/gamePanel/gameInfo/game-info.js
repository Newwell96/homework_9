import React from "react";
import './game-info.css';
import MyPlayerInfo from "./components/game-info-player";

function MyGameInfo (
    {
        playerX,
        playerO
    }
) {
    return (
        <div id="subject-list">
            <h1>Игроки</h1>
            <div className="container">
                <MyPlayerInfo
                    pic={playerO.side}
                    playerName={playerO.fio}
                    percentWin={playerO.percentWin + " побед"}
                />
                <MyPlayerInfo
                    pic={playerX.side}
                    playerName={playerX.fio}
                    percentWin={playerX.percentWin + " побед"}
                />
                </div>
            </div>
    )
}

export default MyGameInfo;
