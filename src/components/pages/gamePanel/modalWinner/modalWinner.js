import React, {useState} from 'react';
import './modalWinner.css';
import MyImage from "../../../UI components/image/image";
import MyButton from "../../../UI components/button/button";

const MyModalWinner = (
    {
        active,
        setActive,
        winner
    }
) => {

    function refreshPage() {
        window.location.reload();
    }

    return (
        <div
            className={active ? "modal active": "modal"}
        >
            <div
                className={active ? "modal-winner show": "modal-winner"}>
                <MyImage
                    name="trophy"
                    width={132}
                    height={165.36}
                />
                <div
                    className="winner-text"
                >
                    {winner} победил!
                </div>
                <MyButton
                    className="button background-default-green"
                    onClickFunction = {refreshPage}
                    buttonText="Новая игра"
                />
                <MyButton
                    className="button back-menu"
                    onClickFunction = {
                        () => setActive(false)
                    }
                    buttonText="Закрыть окно"
                />
            </div>
        </div>
    );
};

export default MyModalWinner;
