import React, {useEffect, useState} from 'react';
import "./game-board.css"
import MyCell from "./components/cell/cell";
import MyTimer from "./components/gameTimer/timer";
import GameInfoMove from "./components/gameInfoMove/game-info-move";
import MyModalWinner from "../modalWinner/modalWinner";

function MyBoard(
    {
        playerO,
        playerX
    }
) {

    const [squares, setSquares] = useState(Array(9).fill(null));
    const [xIsNext, setXIsNext] = useState(true);

    const [isGameRunning, setIsGameRunning] = useState(true);
    const [winner, setWinner] = useState(null);
    const [winnerName, setWinnerName] = useState(null);

    const [winnerCombination, setWinnerCombination]  = useState([]);

    const [firstNameX, lastNameX] = playerX.fio.split(' ').map(name => name.trim());
    const [firstNameO, lastNameO] = playerO.fio.split(' ').map(name => name.trim());

    const [modalActive, setModalActive] = useState(false);

    useEffect(
        () => {
            calculateWinnerName();
        },
        [winner]
    );

    useEffect(
        () => {
            if (squares.includes(null)) {}
            else {
                setIsGameRunning(false)
                setModalActive(true);
            }
        },
        [squares]
    );


    const handleStopTimer = () => {
        setIsGameRunning(false);
    };

    function handleClick(i) {
        if (winner || squares[i]) {
            return;
        }
        const newSquares = squares.slice();
        newSquares[i] = xIsNext ? 'X' : 'O';
        setSquares(newSquares);
        setXIsNext(!xIsNext);
        calculateWinner(newSquares);
    }

    function calculateWinner(squares) {
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
        ];
        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
                setWinnerCombination(lines[i]);
                setWinner(squares[a]);
                setIsGameRunning();
                setModalActive(true);
                return;
            }
        }
    }

    function calculateWinnerName () {
        if (playerX.side === winner) {
            setWinnerName(nameX)
        }
        else if (playerO.side === winner) {
            setWinnerName(nameO)
        }
        else
            return setWinnerName("Сопернический дух")
    }

    function renderSquare(i) {
        let className = "cell";
        if (winnerCombination.includes(i)) {
            if (winner === "X") {
                className = "cell winX"
            }
            else if (winner === "O") {
                className = "cell winO"
            }
        }

        return (
            <MyCell
                className = {className}
                value={squares[i]}
                onClick={() => handleClick(i)}
            />
        );
    }

    const nameX = firstNameX +" "+ lastNameX
    const nameO = firstNameO +" "+ lastNameO

    function renderGameInfoMove() {
        let side;
        let name;

        if (xIsNext) {
            side = "X"
            name = nameX
        }
        else {
            side = "O"
            name = nameO
        }
        return (
            <GameInfoMove
                side={side}
                name={name}
            />
        );
    }

    return (

        <div id="game-container">
            <MyTimer
                isRunning={isGameRunning}
            />
            <div
                id= "game-board">
                {renderSquare(0)}
                {renderSquare(1)}
                {renderSquare(2)}
                {renderSquare(3)}
                {renderSquare(4)}
                {renderSquare(5)}
                {renderSquare(6)}
                {renderSquare(7)}
                {renderSquare(8)}
            </div>
                {renderGameInfoMove()}
            <MyModalWinner
                active={modalActive}
                setActive={setModalActive}
                winner = {winnerName}
            />
        </div>
    );
}

export default MyBoard;
