import React, {useEffect, useState} from "react";
import "./timer.css"

const MyTimer = (
    {
        isRunning,
    }
) => {
    const [seconds, setSeconds] = useState(0);

    useEffect(() => {
        if (!isRunning) {
            return;
        }
        const interval = setInterval(() => {
            setSeconds(seconds => seconds + 1);
        }, 1000);

        return () => clearInterval(interval);
    }, [isRunning]);

    const minutes = Math.floor(seconds / 60);
    const remainingSeconds = seconds % 60;
    const formattedTime = `${minutes < 10 ? '0' : ''}${minutes}:${remainingSeconds < 10 ? '0' : ''}${remainingSeconds}`;

    return (
        <div
            id="game-time"
        >
            {formattedTime}
        </div>
    );
};

export default MyTimer;