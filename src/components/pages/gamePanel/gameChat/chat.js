import React from 'react';
import "./chat.css"
import MyInput from "../../../UI components/input/input";
import MyButton from "../../../UI components/button/button";
import MyMessage from "./components/message-chat";

const MyChat = () => {
    return (
        <div id="chat-container">

            <div className="msgs-container">
                <MyMessage
                    messageSide="X"
                    name="Плюшкина Екатерина"
                    time="13:40"
                    messageText="Ну что, готовься к поражению!!1"
                />

                <MyMessage
                    messageSide="O"
                    name="Пупкин Владлен"
                    time="13:41"
                    messageText="Надо было играть за крестики. Розовый — мой не самый счастливый цвет"
                />

                <MyMessage
                    messageSide="O"
                    name="Пупкин Владлен"
                    time="13:45"
                    messageText="Я туплю…"
                />

                <MyMessage
                    messageSide="X"
                    name="Плюшкина Екатерина"
                    time="13:47"
                    messageText="Отойду пока кофе попить, напиши в тг как сходишь"
                />
            </div>

            <div className="msg-interactive-elements">

                <MyInput
                    className="text-field__input"
                    placeholder="Сообщение..."
                />
                <MyButton
                    className="button"
                    imageName="send"
                    buttonID={"submit"}
                />
            </div>
        </div>
    );
};

export default MyChat;