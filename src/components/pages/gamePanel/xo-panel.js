import React from 'react';
import './xo-panel.css';
import MyGameInfo from "./gameInfo/game-info";
import MyChat from "./gameChat/chat";
import MyBoard from "./gameBoard/game-board";

function XOPanel() {

    const users = [{
        fio: 'Пупкин Владлен Игоревич',
        side: "O",
        percentWin: 63
    }, {
        fio: 'Плюшкина Екатерина Викторовна',
        side: "X",
        percentWin: 23
    }]

    return (

        <div id="main-container">

            <MyGameInfo
                playerO={users[0]}
                playerX={users[1]}
            />

            <MyBoard
                playerO={users[0]}
                playerX={users[1]}
            />

            <MyChat/>

        </div>)
}

export default XOPanel;