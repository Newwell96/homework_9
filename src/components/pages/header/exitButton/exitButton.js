import React, {useState} from 'react';
import MyImage from "../../../UI components/image/image";
import "./exitButton.css"

const ExitButton = (
    {onClickFunction}
) => {
    const [buttonState, setButtonState] = useState("default")

    const handleMouseEnter = () => {
        setButtonState("hovered");
    }

    const handleMouseLeave = () => {
        setButtonState("default");
    }

    let imageName;
    if (buttonState==="default") {
        imageName = "signOut"
    }
    else {
        imageName = "signOutHovered"
    }

    return (
        <div
            className="exit-button-container"
        >
        <button
            className="exit-button"
            onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseLeave}
            onClick={onClickFunction}
        >
            <MyImage
                name={imageName}
            />
        </button>
        </div>
    );
};

export default ExitButton;
