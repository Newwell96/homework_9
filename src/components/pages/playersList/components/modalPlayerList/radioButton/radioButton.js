import React from 'react';
import MyImage from "../../../../../UI components/image/image";
import "./radioButton.css"

const RadioButton = (
    {name},
) => {
    return (
        <div>
            <input
                className="radio-input"
                type="radio"
                id={name}
                name="myRadioGroup"
                value={name}
            />
            <label
                className="radio-container"
                htmlFor={name}
            >
                <MyImage
                    name={name}
                    width={32}
                    height={32}
                />
            </label>

        </div>
    );
};

export default RadioButton;