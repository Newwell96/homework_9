import React from 'react';
import PlayerWinnerLoser from "./playerWinnerUser/player-winner-loser";
import "../../../../../index.css"
import "./gameHistoryRow.css"

const GameHistoryRow = (props) => {

    let winX;
    let winO;

    if (props.winner === "X") {
        winX = "win"
    }
    else if (props.winner === "O") {
        winO = "win"
    }

    return (
        <tr className="history_row">
            <td className="players_colomn">

                <PlayerWinnerLoser
                    playerName ={props.nameO}
                    playerSide = "O"
                    playerStatus = {winO}
                />
                <h3>
                    против
                </h3>
                <PlayerWinnerLoser
                    playerName ={props.nameX}
                    playerSide = "X"
                    playerStatus = {winX}
                />
            </td>
            <td className="date_colomn">
                {props.date}
            </td>
            <td className="gameTimeColomn">
                {props.gameTime}
            </td>
        </tr>
    );
};

export default GameHistoryRow;